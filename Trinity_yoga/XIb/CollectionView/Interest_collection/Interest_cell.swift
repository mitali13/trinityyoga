//
//  Interest_cell.swift
//  Trinity_yoga
//
//  Created by Mandeeppc on 20/03/20.
//  Copyright © 2020 DECODER. All rights reserved.
//

import UIKit

class Interest_cell: UICollectionViewCell {

    @IBOutlet var interestLabel: UILabel!
    @IBOutlet var astrologyImage: UILabel!
    @IBOutlet var checkImage: UIImageView!
    @IBOutlet weak var astrologyimages: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        astrologyimages.layer.opacity = 0.8
        astrologyimages.layer.cornerRadius = 8
        astrologyimages.clipsToBounds = true
        
        checkImage.clipsToBounds = true
        checkImage.layer.cornerRadius = checkImage.frame.size.height / 2
        // Initialization code
    }
    
    func configCell(with model: Interests){
        //interestLabel.text = model.optionName
        
        if model.isChecked == true{
            checkImage.isHidden = false
        }else{
            checkImage.isHidden = true
        }
    }
    
    

}
