//
//  BrowserCell.swift
//  Trinity_yoga
//
//  Created by DECODER on 20/10/20.
//  Copyright © 2020 DECODER. All rights reserved.
//

import UIKit

class BrowserCell: UITableViewCell {
   
    @IBOutlet weak var lblBwoserName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        //Initialization code
        lblBwoserName.layer.cornerRadius = 8
        lblBwoserName.layer.borderColor = UIColor.black.cgColor
       lblBwoserName.layer.shadowColor = UIColor.lightGray.cgColor
        lblBwoserName.layer.shadowOffset = CGSize(width: 0, height: 1)
        lblBwoserName.layer.borderWidth = 0.7
        lblBwoserName.layer.borderColor = UIColor.black.cgColor
        lblBwoserName.clipsToBounds = true
        lblBwoserName.layer.shadowRadius = 3
        lblBwoserName.layer.shadowOpacity = 1
        lblBwoserName.layer.masksToBounds = false;
        lblBwoserName.font = UIFont(name: "Ubuntu", size: 21)
    }
}
