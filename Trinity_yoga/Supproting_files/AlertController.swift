//
//  AlertController.swift
//  Vibrant
//
//  Created by Admin on 23/10/18.
//  Copyright © 2018 VH. All rights reserved.
//

import UIKit

class RDAlertController
{
    class var shared:RDAlertController
    {
        struct sharedController
        {
            static var controller = RDAlertController()
        }
        return sharedController.controller
    }
    
    func simpleAlert(with title:String?,message:String?,presentOn vc:UIViewController)
    {
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction.init(title: "OK", style: .default, handler: nil)
        alert.addAction(action)
        vc.present(alert, animated: true, completion: nil)
    }
    
    func noInternet(presentOn vc:UIViewController)
    {
        let alert = UIAlertController.init(title: "No Internet", message: nil, preferredStyle: .alert)
        let Settingsaction = UIAlertAction.init(title: "Settings", style: .default) { (action) in
//            if UIApplication.shared.canOpenURL(URL.init(string: UIApplication.openSettingsURLString)!)
//            {
//                UIApplication.shared.open(URL.init(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
//            }
        }
        let close = UIAlertAction.init(title: "Close", style: .cancel, handler: nil)
        alert.addAction(Settingsaction)
        alert.addAction(close)
        vc.present(alert, animated: true, completion: nil)
    }
    func ServerError(presentOn vc:UIViewController,retry:(()->())?)
    {
        let alert = UIAlertController.init(title: "OOPS! server error", message: nil, preferredStyle: .alert)
        if let ret = retry
        {
            let retryaction = UIAlertAction.init(title: "Retry", style: .default) { (action) in
                ret()
            }
            alert.addAction(retryaction)
        }
    
        let close = UIAlertAction.init(title: "Close", style: .cancel, handler: nil)
        alert.addAction(close)
        vc.present(alert, animated: true, completion: nil)
    }
}
