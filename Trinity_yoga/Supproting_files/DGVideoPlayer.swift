//
//  DGVideoPlayer.swift
//  Playground
//
//  Created by Dhruv Govani on 21/07/20.
//  Copyright © 2020 Dhruv Govani. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

class DGVideoPlayer: UIView {
    var player: AVPlayer? {
        get {
            return playerLayer.player
        }

        set {
            playerLayer.player = newValue
        }
    }

    var playerLayer: AVPlayerLayer {
        return layer as! AVPlayerLayer
    }

    override class var layerClass: AnyClass {
        return AVPlayerLayer.self
    }
}
