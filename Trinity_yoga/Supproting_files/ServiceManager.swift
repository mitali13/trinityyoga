//
//  ServiceManager.swift
//  CLM
//
//  Created by DEVIL DECODER on 18/08/18.
//  Copyright © 2018 DECODER. All rights reserved.
//

import Foundation
import Alamofire
import Reachability
import MBProgressHUD


class ServiceManager
{
    
    
    var header:HTTPHeaders!
    
    
    private(set) var Url =  "http://work-demo.xyz/Property_vault/"
    private(set) var ClientVersion = "public/api/v1/"
//    private(set) var BusinessVersion = "Service_business/"
//    private(set) var TourUploads = "uploads/tour/"
//    private(set) var CompanyLogo = "uploads/company/logo/"
    private let app = UIApplication.shared.delegate as! AppDelegate
    typealias CompletionHandler = ([String:Any]?)->Void
    
    static var shared:ServiceManager{
        get{
            struct serviceManager {
                static let manager = ServiceManager()
            }
            return serviceManager.manager
        }
    }
    
    
    let progress = UIActivityIndicatorView.init(style: UIActivityIndicatorView.Style.white)
    
    var RootViewController:UIViewController? = {
        
        if #available(iOS 13.0, *)
        {
            let Scenes = UIApplication.shared.connectedScenes
            for scene in Scenes
            {
                if scene.activationState == .foregroundActive
                {
                    let vc = ((scene as! UIWindowScene).delegate as! UIWindowSceneDelegate).window!!.rootViewController
                    if let rootVC = vc{
                        return rootVC
                    }
                    else
                    {
                        return nil
                    }
                }
            }
            return nil
        }
        else
        {
            let app = UIApplication.shared.delegate as! AppDelegate
            return app.window?.rootViewController
        }
    }()
    
    let manager:SessionManager!
    
    init() {
        
        let config = URLSessionConfiguration.default
        config.requestCachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        config.urlCache = nil
      
        manager = Alamofire.SessionManager(configuration: config)
        
        
    }
    
    func MakeApiCall(ForServiceName serviceName:String,withParameters Parameters:[String:Any]?,withAttachments Attachments:[Any]?,withAttachmentName:[String]?,UploadParameter:String?,httpMethod:HTTPMethod,ShowLoader:Bool,ShowTrueFalseValue:Bool,RetryMethod:(()->())?,URLisStatic:Bool? = false,completionHandler:@escaping CompletionHandler)
    {
        
        var url = ""
        if URLisStatic == false
        {
          url = "\(Url)\(ClientVersion)\(serviceName)"
        }
        else
        {
            url = serviceName
        }
        print(url)
        print(Parameters)
        
        if let attachments = Attachments {
            if (NetworkReachabilityManager()?.isReachable)!{
                if ShowLoader
                {
                    let loadingNotification = MBProgressHUD.showAdded(to: UIApplication.shared.keyWindow!.rootViewController!.view, animated: true)
                    loadingNotification.mode = MBProgressHUDMode.indeterminate
                    loadingNotification.label.text = "Loading"
                    
//                    self.ShowProgess()
                }
                
                if UserDefaults.standard.value(forKey: "token") as? String ?? "" != "" {
                    header = ["Authorization":"Bearer " + "\(UserDefaults.standard.value(forKey: "token") as? String ?? "")","Content-Type": "application/json"]
                }else{
                    header = nil
                }
                
                manager.upload(multipartFormData: { (multipart) in
                    for Attachment in attachments
                    {
                        let randome = arc4random()
                        
                           let data = (Attachment as! UIImage).jpegData(compressionQuality: 0.75)
                            multipart.append(data!, withName: UploadParameter!, fileName: "randome\(randome).jpg", mimeType: "image/jpg")
                    
                    }
                    if let params = Parameters{
                        print(Parameters!)

                        for (key,value) in params{
                            multipart.append("\(value)".data(using: String.Encoding.utf8)!, withName: "\(key)")
                        }
                    }

                }, usingThreshold: UInt64.init(), to: URL.init(string: url)!, method: httpMethod, headers: header, queue: nil) { (Responce) in
                    switch Responce {
                    case .success(let upload, _, _):
                        upload.responseJSON { Responce in
                            if Responce.error == nil
                            {
                                
                                if ShowTrueFalseValue == false
                                {
                                    guard let responce = Responce.result.value else{return}
                                    guard let dic = responce as? [String:Any] else{return}
                                    
                                    if dic["status"] as? String ?? "" == "1"{
                                        if ShowLoader{
                                            MBProgressHUD.hide(for: UIApplication.shared.keyWindow!.rootViewController!.view, animated: true)
//                                            completionHandler(data)
//                                            KVNProgress.dismiss(completion: {
                                                guard let data = dic["data"] as? [String:Any] else {return}
                                                completionHandler(data)
//                                            })
                                        }
                                        else
                                        {
                                            guard let data = dic["data"] as? [String:Any] else {return}
                                            completionHandler(data)
                                        }
                                    }
                                    else
                                    {
                                        if ShowLoader{
                                            MBProgressHUD.hide(for: UIApplication.shared.keyWindow!.rootViewController!.view, animated: true)
                                                                                   
                                                                                   
                                            guard let vc = self.app.window?.rootViewController else {return}
                                            RDAlertController.shared.simpleAlert(with: dic["message"] as? String ?? "", message: nil, presentOn: vc)
                                        }
                                        else
                                        {
                                            guard let vc = self.app.window?.rootViewController else {return}
                                            RDAlertController.shared.simpleAlert(with: dic["message"] as? String ?? "", message: nil, presentOn: vc)
                                        }
                                    }
                                }
                                else if ShowTrueFalseValue == true
                                {
                                    if ShowLoader{
                                        MBProgressHUD.hide(for: UIApplication.shared.keyWindow!.rootViewController!.view, animated: true)
                                        
                                        guard let responce = Responce.result.value else{return}
                                        guard let dic = responce as? [String:Any] else{return}
                                        completionHandler(dic)
                                    }
                                    else
                                    {
                                        guard let responce = Responce.result.value else{return}
                                        guard let dic = responce as? [String:Any] else{return}
                                        completionHandler(dic)
                                    }
                                }
                            }
                            else
                            {
                                if ShowLoader{
                                    MBProgressHUD.hide(for: UIApplication.shared.keyWindow!.rootViewController!.view, animated: true)
                                    
                                    guard let vc = self.app.window?.rootViewController else {return}
                                    RDAlertController.shared.ServerError(presentOn: vc, retry: RetryMethod)
                                }
                                else
                                {
                                    guard let vc = self.app.window?.rootViewController else {return}
                                    RDAlertController.shared.ServerError(presentOn: vc, retry: RetryMethod)
                                }
                            }
                        }
                    case .failure(let _):
                        if ShowLoader{
                            MBProgressHUD.hide(for: UIApplication.shared.keyWindow!.rootViewController!.view, animated: true)
                            
                            guard let vc = self.app.window?.rootViewController else {return}
                            RDAlertController.shared.ServerError(presentOn: vc, retry: RetryMethod)
                        }
                        else
                        {
                            guard let vc = self.app.window?.rootViewController else {return}
                            RDAlertController.shared.ServerError(presentOn: vc, retry: RetryMethod)
                        }
                    }
                }
            }
            else
            {
                
                guard let vc = app.window?.rootViewController else {return}
                RDAlertController.shared.noInternet(presentOn: vc)
            }
        }
            
        else
        {
            if (NetworkReachabilityManager()?.isReachable)!{
                if ShowLoader
                {
                    let loadingNotification = MBProgressHUD.showAdded(to: UIApplication.shared.keyWindow!.rootViewController!.view, animated: true)
                                   loadingNotification.mode = MBProgressHUDMode.indeterminate
                                   loadingNotification.label.text = "Loading"
                }
            //    manager.request(URL.init(string: url)!, method: httpMethod, parameters: Parameters, encoding:  ParameterEncoding, headers: )
                // ,headers : Header
                
                if UserDefaults.standard.value(forKey: "token") as? String ?? "" != "" {
                    header = ["Authorization":"Bearer " + "\(UserDefaults.standard.value(forKey: "token") as? String ?? "")","Content-Type": "application/json"]
                }else{
                    header = nil
                }
                
                manager.request(URL.init(string: url)!, method: httpMethod, parameters: Parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (Responce) in
                    
                    if Responce.error == nil
                    {
                        if ShowTrueFalseValue == false
                        {
                        guard let responce = Responce.result.value else{return}
                        guard let dic = responce as? [String:Any] else{return}
                        
                        if dic["status"] as? String ?? "" == "1"{
                            if ShowLoader{
                                 MBProgressHUD.hide(for: UIApplication.shared.keyWindow!.rootViewController!.view, animated: true)
                                
                                guard let data = dic["data"] as? [String:Any] else {return}
                                completionHandler(data)
                            }
                            else
                            {
                                guard let data = dic["data"] as? [String:Any] else {return}
                                completionHandler(data)
                            }
                        }
                        else
                        {
                            if ShowLoader{
                                 MBProgressHUD.hide(for: UIApplication.shared.keyWindow!.rootViewController!.view, animated: true)
                                
                                guard let vc = self.app.window?.rootViewController else {return}
                                RDAlertController.shared.simpleAlert(with: dic["message"] as? String ?? "", message: nil, presentOn: vc)
                            }
                            else
                            {
                                guard let vc = self.app.window?.rootViewController else {return}
                                RDAlertController.shared.simpleAlert(with: dic["message"] as? String ?? "", message: nil, presentOn: vc)
                            }
                        }
                        }
                        else if ShowTrueFalseValue == true
                        {
                            if ShowLoader{
                                 MBProgressHUD.hide(for: UIApplication.shared.keyWindow!.rootViewController!.view, animated: true)
                                
                                guard let responce = Responce.result.value else{return}
                                guard let dic = responce as? [String:Any] else{return}
                                completionHandler(dic)
                            }
                            else
                            {
                                guard let responce = Responce.result.value else{return}
                                guard let dic = responce as? [String:Any] else{return}
                                completionHandler(dic)
                            }
                        }
                    }
                    else
                    {
                        if ShowLoader{
                             MBProgressHUD.hide(for: UIApplication.shared.keyWindow!.rootViewController!.view, animated: true)
                            
                            guard let vc = self.app.window?.rootViewController else {return}
                            RDAlertController.shared.ServerError(presentOn: vc, retry: RetryMethod)
                        }
                        else
                        {
                            guard let vc = self.app.window?.rootViewController else {return}
                            RDAlertController.shared.ServerError(presentOn: vc, retry: RetryMethod)
                        }
                    }
                }
            }
            else
            {
                guard let vc = app.window?.rootViewController else {return}
                RDAlertController.shared.ServerError(presentOn: vc, retry: RetryMethod)
            }
        }
    }
    
    
    private func ShowProgess()
    {
        let ProgessView = UIViewController.init()
        progress.hidesWhenStopped = true
        progress.color = UIColor.white
        progress.layer.cornerRadius = 10
        progress.frame = CGRect.init(x: 0, y: 0, width: 50, height: 50)
        progress.backgroundColor = UIColor.lightGray
        progress.center = ProgessView.view.center
        ProgessView.view.addSubview(self.progress)
        progress.startAnimating()
        ProgessView.modalPresentationStyle = .custom
        let transitinDelegate = RDProgressView.init()
        ProgessView.transitioningDelegate = transitinDelegate
        RootViewController?.present(ProgessView, animated: false, completion: nil)
        
    }
    
    private func DismissProgress()
    {
        progress.stopAnimating()
        RootViewController?.dismiss(animated: false, completion: nil)
    }
    
}
