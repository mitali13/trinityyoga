//
//  rdtextfiled.swift
//  Beauty spa onwer
//
//  Created by Decoder on 12/08/19.
//  Copyright © 2019 Decoder. All rights reserved.
//

import Foundation

import UIKit

@IBDesignable
class rdtextField: UITextField {
    
    @IBInspectable var leftImage:UIImage?{
        didSet{
            if let image = leftImage{
                self.leftViewMode = .always
                let image = UIImageView.init(image: image)
                image.contentMode = .scaleAspectFit
                image.frame = CGRect(x: 20, y: 15, width: 20, height: 20)
                self.leftView = image
                self.layoutIfNeeded()
            }
        }
    }
    
    @IBInspectable var RightImage:UIImage?{
        didSet{
            if let image = RightImage{
                self.rightViewMode = .always
                let image = UIImageView.init(image: image)
                image.contentMode = .scaleAspectFit
                image.frame = CGRect(x: 15, y: 15, width: 13, height: 13)
                self.rightView = image
                self.layoutIfNeeded()
            }
        }
    }
    
    @IBInspectable var padding = 20
    
    @IBInspectable var txtpadding:CGFloat = 40
    
    var CornerRadias = 8
    
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        var rect = super.leftViewRect(forBounds: bounds)
        rect.origin.x = rect.origin.x + CGFloat(padding) - 5
        return rect
    }
    
    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        var rect = super.rightViewRect(forBounds: bounds)
        rect.origin.x = rect.origin.x - CGFloat(padding) - 12
        return rect
    }
    
    let pading = UIEdgeInsets(top: 0, left: 50 , bottom: 0, right: 5)
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: pading)
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: pading)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return  bounds.inset(by: pading)
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.borderWidth = 0.8
        self.layer.borderColor = UIColor.lightGray.cgColor
    }
    
}
