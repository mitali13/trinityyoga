//
//  Utils.swift
//  Finca
//
//  Created by anjali on 01/06/19.
//  Copyright © 2019 anjali. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

class Utils: NSObject {
    
    static func setRoundImageWithBorder(imageView:UIImageView , color:UIColor){
        imageView.layer.cornerRadius = imageView.frame.width / 2
        imageView.clipsToBounds = true
        imageView.layer.borderColor = color.cgColor
        imageView.layer.borderWidth = 2
    }
    
    static func setImageFromUrl(imageView:UIImageView , urlString:String) {
        // print("utils kf string : ==== "+urlString)
        let url = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        imageView.kf.setImage(
            with: URL(string: url!),
            placeholder: UIImage(named: "placeholder"),
            options: [])
        {
            result in
            switch result {
            case .success( _):
                // print("Task done for: \(value.source.url?.absoluteString ?? "")")
                
                break
            case .failure(let error):
                print("Job failed: \(error.localizedDescription)")
                
                break
            }
        }
    }
    
    static func setImageFromUrl(imageView:UIImageView , urlString:String ,palceHolder:String) {        
        let url = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        imageView.kf.setImage(
            with: URL(string: url!),
            placeholder: UIImage(named: palceHolder),
            options: [])
        {
            result in
            switch result {
            case .success( _):
                // print("Task done for: \(value.source.url?.absoluteString ?? "")")
                
                break
            case .failure(let error):
                print("Job failed: \(error.localizedDescription)")
                
                break
            }
        }
    }
    
  
    static func changeDateFormat() -> DateFormatter {
        let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy"
       return formatter
    }
    
}
