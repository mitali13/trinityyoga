//
//  BreathingVC.swift
//  Trinity_yoga
//
//  Created by Mandeeppc on 20/03/20.
//  Copyright © 2020 DECODER. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
struct DropdownOptions{
    let option : String
}
class BreathingVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
    
    @IBOutlet var optionTableViewHeught: NSLayoutConstraint!
    @IBOutlet var optionTableView: UITableView!
    @IBOutlet weak var txtpopular: rdtextField!
    @IBOutlet weak var breathingtableview: UITableView!
    var player : AVPlayer!
    var playercontroller = AVPlayerViewController()
    var isDropMenuHidden : Bool = true
    var optionArr = [DropdownOptions]()
    var breathingArr = [YogaTutorialData]()
    let user_id = UserDefaults.standard.object(forKey: LoginuserId)
    var category_type = 0
    var like_type: Int!
    var videoId : Int!
    var tot_like,tot_dislike,my_like,my_unlike : Int!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let navLabel = UILabel()
        let navTitle = NSMutableAttributedString(string: "Breathing", attributes:[
            NSAttributedString.Key.foregroundColor: UIColor.black,
            NSAttributedString.Key.font: UIFont(name: "Ubuntu", size: 23)!])
        navLabel.attributedText = navTitle
        self.navigationItem.titleView = navLabel
        txtpopular.text = "All"
        optionArr.append(DropdownOptions(option: "All"))
        optionArr.append(DropdownOptions(option: "Most Popular"))
        optionArr.append(DropdownOptions(option: "Most Recent"))
        breathingtableview.delegate = self
        breathingtableview.dataSource = self
        optionTableView.delegate = self
        optionTableView.dataSource = self
        optionTableViewHeught.constant = 0
        txtpopular.setIcon(#imageLiteral(resourceName: "down"))
        txtpopular.appendTextfield()
        txtpopular.layer.cornerRadius = 10
        txtpopular.clipsToBounds = true
        txtpopular.delegate = self
        breathingtableview.register(UINib(nibName: "TutorialVideoTableViewCell", bundle: nil), forCellReuseIdentifier: "TutorialCell")
        optionTableView.register(UINib(nibName: "Topicscell", bundle: nil), forCellReuseIdentifier: "Topicscell")
       //Uncomment two lines for drop down menu
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(showMenu))
        txtpopular.addGestureRecognizer(tap)
        
        navigationController?.SetupNavWithLeftRightImage(onVc: self, WithTitle: "Breathing", leftImage: #imageLiteral(resourceName: "back"), RightImage: nil, leftAction: #selector(backTapped), Image: nil)
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        apiGetSubcategory()
    }
    func apiGetLikeDislike(){
        let params = ["user_id": user_id!,"video_id" : videoId!,"type" : like_type!]
        ServiceManager.shared.MakeApiCall(ForServiceName: "likeAndUnlike", withParameters: params, withAttachments: nil, withAttachmentName: nil, UploadParameter: nil, httpMethod: .get, ShowLoader: true, ShowTrueFalseValue: true, completionHandler: { (response) in
            let status = response?[0]["status"] as? Int ?? 0
            let error_msg = response?[0]["error_msg"] as? String ?? ""
            if status == 1{
                self.apiGetSubcategory()
            }else{
                UIAlertController().Simplealert(withTitle: error_msg, Message: "", presentOn: self)
            }
        }, with: nil)
    }
    func apiGetSubcategory(){
        let category_id = UserDefaults.standard.object(forKey: "category_id")
        let sub_category_id = UserDefaults.standard.object(forKey: "sub_category_id")
        let params = ["user_id": user_id!, "category_id" : category_id!, "sub_category_id" : sub_category_id!, "type" : category_type]
            ServiceManager.shared.MakeApiCall(ForServiceName: "GetSubCategoryData", withParameters: params, withAttachments: nil, withAttachmentName: nil, UploadParameter: nil, httpMethod: .get, ShowLoader: true, ShowTrueFalseValue: true, completionHandler: { (response) in
                let status = response?[0]["status"] as? Int ?? 0
                let error_msg = response?[0]["error_msg"] as? String ?? ""
                if status == 1{
                    do{
                        let jsonData = try JSONSerialization.data(withJSONObject: response?[0] ?? [:], options: [])
                        let decode = try JSONDecoder().decode(YogaTutorialResponse.self, from: jsonData)
                        self.breathingArr = decode.data!
                        self.breathingtableview.reloadData()
                    }catch let error as NSError{
                        print(error.localizedDescription)
                    }
                }else{
                    UIAlertController().Simplealert(withTitle: error_msg, Message: "", presentOn: self)
                }
            }, with: nil)
        
    }
    func apiAddToRecentview(){
        let params = ["user_id": user_id!,"video_id" : videoId!]
        ServiceManager.shared.MakeApiCall(ForServiceName: "addToRecentlyViewed", withParameters: params, withAttachments: nil, withAttachmentName: nil, UploadParameter: nil, httpMethod: .get, ShowLoader: true, ShowTrueFalseValue: true, completionHandler: { (response) in
            print(response!)
            let status = response?[0]["status"] as? Int ?? 0
            let error_msg = response?[0]["error_msg"] as? String ?? ""
            if status == 1{
                self.apiGetSubcategory()
            }else{
               UIAlertController().Simplealert(withTitle: error_msg, Message: "", presentOn: self)
            }
        }, with: nil)
    }
    @objc func likeAction(sender : UIButton){
        videoId = breathingArr[sender.tag].id
        my_like = breathingArr[sender.tag].mylike
        if my_like == 0{
            like_type = 1
            apiGetLikeDislike()
        }
        else if my_like == 1{
            like_type = 0
            apiGetLikeDislike()
        }
    }
    @objc func dislikeAction(sender : UIButton){
        videoId = breathingArr[sender.tag].id
        my_unlike = breathingArr[sender.tag].myunlike
        if my_unlike == 0{
            like_type = 2
            apiGetLikeDislike()
        }
        else if my_unlike == 1{
            like_type = 0
            apiGetLikeDislike()
        }
    }
    @objc func backTapped(){
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    @objc func showMenu(){
        if isDropMenuHidden == true{
            UIView.animate(withDuration: 0.2, animations: {
                self.optionTableViewHeught.constant = 90
                self.view.layoutIfNeeded()
            })
            isDropMenuHidden = false
        }else{
            UIView.animate(withDuration: 0.2, animations: {
                self.optionTableViewHeught.constant = 0
                self.view.layoutIfNeeded()
            })
            isDropMenuHidden = true
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            if textField == txtpopular {
                return false; //do not show keyboard nor cursor
            }
            return true
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == optionTableView{
            return optionArr.count
        }
        if tableView == breathingtableview{
            return breathingArr.count
        }
        return 0
    }
    @objc func playAction(_ sender : UIButton){
        videoId = breathingArr[sender.tag].id
        apiAddToRecentview()

       let vc = self.storyboard?.instantiateViewController(identifier: "mediaplayervc")as! mediaplayervc
        self.navigationController?.isNavigationBarHidden = true
        let urlStr = breathingArr[sender.tag].video
        vc.vdoUrl = videoUrl + urlStr!
        vc.video_id = breathingArr[sender.tag].id
        vc.my_like = breathingArr[sender.tag].mylike
        vc.my_unlike = breathingArr[sender.tag].myunlike
        vc.tot_like = breathingArr[sender.tag].totallike
        vc.tot_unlike = breathingArr[sender.tag].totalunlike
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == optionTableView{
            let cell = optionTableView.dequeueReusableCell(withIdentifier: "Topicscell") as! Topicscell
            cell.topicnamelbl.text = optionArr[indexPath.row].option
            return cell
        }
      
        let cell = breathingtableview.dequeueReusableCell(withIdentifier: "TutorialCell", for: indexPath) as! TutorialVideoTableViewCell
        cell.videoDescription.text = breathingArr[indexPath.row].description
        cell.videoTitle.text = breathingArr[indexPath.row].title
        cell.videoDuration.text = breathingArr[indexPath.row].videoduration
        tot_like = breathingArr[indexPath.row].totallike
        tot_dislike = breathingArr[indexPath.row].totalunlike
        my_like = breathingArr[indexPath.row].mylike
        my_unlike = breathingArr[indexPath.row].myunlike
        if my_like == 1{
            cell.likeButton.setImage(#imageLiteral(resourceName: "fill_like"), for: .normal)
        }else{
              cell.likeButton.setImage(#imageLiteral(resourceName: "like"), for: .normal)
        }
        if my_unlike == 1{
            cell.deslikeButton.setImage(#imageLiteral(resourceName: "fill_dislike"), for: .normal)
        }else{
              cell.deslikeButton.setImage(#imageLiteral(resourceName: "dislike"), for: .normal)
        }
        let urlStr = breathingArr[indexPath.row].video
        let vdoUrl = videoUrl + urlStr!
        print(vdoUrl)
        let videopathurl = URL(string : vdoUrl)
        let asset = AVURLAsset(url: videopathurl!, options: nil)
        let generator = AVAssetImageGenerator(asset: asset)
        generator.appliesPreferredTrackTransform = true
         do {
            cell.videoThumbnail.image = try UIImage(cgImage: generator.copyCGImage(at: CMTime(seconds: 0, preferredTimescale: 1), actualTime: nil))
            } catch let e as NSError {
                    print("Error: \(e.localizedDescription)")
            }
        cell.likeButton.setTitle(String(tot_like), for: .normal)
        cell.deslikeButton.setTitle(String(tot_dislike), for: .normal)
        cell.likeButton.tag = indexPath.row
        cell.deslikeButton.tag = indexPath.row
        cell.likeButton.addTarget(self, action: #selector(likeAction), for: .touchUpInside)
        cell.deslikeButton.addTarget(self, action: #selector(dislikeAction), for: .touchUpInside)
        cell.btnFullScreen.isHidden = true
        cell.playButton.tag = indexPath.row
        cell.playButton.addTarget(self, action: #selector(playAction), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == optionTableView{
            return 30
        }
        if tableView == breathingtableview{
            return UITableView.automaticDimension}
        return 0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
         if tableView == optionTableView{
            if indexPath.row == 0{
                category_type = 0
                apiGetSubcategory()
                txtpopular.text = optionArr[indexPath.row].option
                UIView.animate(withDuration: 0.2, animations: {
                    self.optionTableViewHeught.constant = 0
                    self.view.layoutIfNeeded()
                })
                isDropMenuHidden = true
            }
            else if indexPath.row == 1{
                category_type = 1
                apiGetSubcategory()
                txtpopular.text = optionArr[indexPath.row].option
                UIView.animate(withDuration: 0.2, animations: {
                    self.optionTableViewHeught.constant = 0
                    self.view.layoutIfNeeded()
                })
                isDropMenuHidden = true
            }
            else if indexPath.row == 2{
                category_type = 2
                apiGetSubcategory()
                 txtpopular.text = optionArr[indexPath.row].option
                UIView.animate(withDuration: 0.2, animations: {
                    self.optionTableViewHeught.constant = 0
                    self.view.layoutIfNeeded()
                })
                isDropMenuHidden = true
             
            }
        }
    }
}
