//
//  WelcomeafterloginVC.swift
//  Trinity_yoga
//
//  Created by Mandeeppc on 20/03/20.
//  Copyright © 2020 DECODER. All rights reserved.
//

import UIKit

class WelcomeafterloginVC: UIViewController {
    @IBOutlet weak var logoimg: UIImageView!
    @IBOutlet weak var gettingstartedbtn: UIButton!
    
    @IBOutlet weak var lblName: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        lblName.text = UserDefaults.standard.object(forKey: "name") as? String;  gettingstartedbtn.applyRadialGradient(colours: [buttoncolor,buttoncolor2])
        gettingstartedbtn.setTitleColor(UIColor.white, for: .normal)
        gettingstartedbtn.layer.cornerRadius = gettingstartedbtn.frame.height / 2
        gettingstartedbtn.clipsToBounds = true
        gettingstartedbtn.setTitle("GET STARTED", for: .normal)
        // Do any additional setup after loading the view.
    let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleGasture))
        swipeLeft.direction = .right
        self.view.addGestureRecognizer(swipeLeft)
    }
    @objc func handleGasture(gesture : UISwipeGestureRecognizer){
        if gesture.direction == .right{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func backaction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func gettingstartedaction(_ sender: UIButton) {
        
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "InterestVC") as! InterestVC
               self.navigationController?.pushViewController(vc, animated: true)
    }
}
