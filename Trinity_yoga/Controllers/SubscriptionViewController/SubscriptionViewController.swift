//
//  SubscriptionViewController.swift
//  Trinity_yoga
//
//  Created by Dhruv Govani on 11/04/20.
//  Copyright © 2020 DECODER. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
struct SubscriptionResponse: Decodable {
    let error_msg: String!
    let data: [SubscriptionData]!
    let status: Int!
}

// MARK: - DataClass
struct SubscriptionData: Decodable {
    let SubscriptionDetail: [SubscriptionDetail]!
    let MainDescription: String!
}
struct SubscriptionDetail: Decodable {
    let id: Int!
    let name, price, duration: String!
    let status: Int!
}
class SubscriptionViewController: UIViewController,CustomePickerDelegate,UITextFieldDelegate {
    var subscriptionArr = [SubscriptionDetail]()
    var currency = ["USD $", "AUSD A$", "GBP £", "EUR €"]
    @IBOutlet var packageView: UIView!
    @IBOutlet var packageTitle: UILabel!
    @IBOutlet var packageDuration: UILabel!
    @IBOutlet var packagePrice: UILabel!
    @IBOutlet var monthlyPackageview: UIView!
    
    @IBOutlet weak var txtPrice: UITextField!
    @IBOutlet var Subscriptionsubtitle: UILabel!
    @IBOutlet var monthlyPackageTitle: UILabel!
    @IBOutlet var MonthlypackageDuration: UILabel!
    @IBOutlet var monthlyPackagePric: UILabel!
    var Picker:customePicker!
    
    @IBOutlet weak var descriptionView: UITextView!
    @IBOutlet var specialPackageView: UIView!
    @IBOutlet var specialPackageTitle: UILabel!
    @IBOutlet var specialPackageDuration: UILabel!
    @IBOutlet var specialPackagePrice: UILabel!
    
    var SelectedPlan : Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        apiGetSubscriptionDetail()
        let navLabel = UILabel()
        let navTitle = NSMutableAttributedString(string: "Subscription", attributes:[
            NSAttributedString.Key.foregroundColor: UIColor.black,
            NSAttributedString.Key.font: UIFont(name: "Ubuntu", size: 23)!])
        navLabel.attributedText = navTitle
        self.navigationItem.titleView = navLabel
        let PopularPKGTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(PopularPackageSelected))
        let MothlyPKGTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MonthlyPackageSelected))
        let SpecialPKGTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SpecialPackageSelected))
        
        
        packageView.addGestureRecognizer(PopularPKGTap)
        monthlyPackageview.addGestureRecognizer(MothlyPKGTap)
        specialPackageView.addGestureRecognizer(SpecialPKGTap)
        //Subscriptionsubtitle.text = "Price shown in USD. \nTap to select Plan."
        
        packageDuration.textColor =  UIColor(red: 86/255, green: 193/255, blue: 195/255, alpha: 1)
        specialPackageDuration.textColor = UIColor(red: 86/255, green: 193/255, blue: 195/255, alpha: 1)
        MonthlypackageDuration.textColor = UIColor(red: 86/255, green: 193/255, blue: 195/255, alpha: 1)
        
        
        
        packageTitle.backgroundColor = UIColor(red: 182/255, green: 239/255, blue: 239/255, alpha: 1)
        
        monthlyPackageTitle.backgroundColor = UIColor(red: 182/255, green: 239/255, blue: 239/255, alpha: 1)
        specialPackageTitle.backgroundColor = UIColor(red: 182/255, green: 239/255, blue: 239/255, alpha: 1)
        
        packageTitle.makeMePackageTitle()
        monthlyPackageTitle.makeMePackageTitle()
        specialPackageTitle.makeMePackageTitle()
        packageView.makeMePackage()
        specialPackageView.makeMePackage()
        monthlyPackageview.makeMePackage()

         navigationController?.SetupNavWithLeftRightImage(onVc: self, WithTitle: "Subscriptions", leftImage: #imageLiteral(resourceName: "back"), RightImage: nil, leftAction: #selector(backAction), Image: nil)
        
        packageView.makeMeSelectedPKG(titleLabel: packageTitle)
        
        Picker = Bundle.main.loadNibNamed("CustomePicker", owner: self, options: nil)?.first as? customePicker
        Picker.Delegate = self
        txtPrice.delegate = self
        txtPrice.inputView = Picker
        
    let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleGasture))
           swipeLeft.direction = .right
           self.view.addGestureRecognizer(swipeLeft)
           
    }
    @objc func handleGasture(gesture : UISwipeGestureRecognizer){
           if gesture.direction == .right{
               self.navigationController?.popViewController(animated: true)
    
           }
    }
    @objc func PopularPackageSelected(){
        SelectedPlan = 0
        packageView.makeMeSelectedPKG(titleLabel: packageTitle)
        monthlyPackageview.makeMeUnselectedPKG(titleLabel: monthlyPackageTitle)
        specialPackageView.makeMeUnselectedPKG(titleLabel: specialPackageTitle)
    }
    @objc func MonthlyPackageSelected(){
        SelectedPlan = 1
           monthlyPackageview.makeMeSelectedPKG(titleLabel: monthlyPackageTitle)
           packageView.makeMeUnselectedPKG(titleLabel: packageTitle)
           specialPackageView.makeMeUnselectedPKG(titleLabel: specialPackageTitle)
       }
    @objc func SpecialPackageSelected(){
        SelectedPlan = 2
           specialPackageView.makeMeSelectedPKG(titleLabel: specialPackageTitle)
           monthlyPackageview.makeMeUnselectedPKG(titleLabel: monthlyPackageTitle)
           packageView.makeMeUnselectedPKG(titleLabel: packageTitle)
       }
    override func viewDidDisappear(_ animated: Bool) {
        self.title = ""
    }
    @objc func backAction(){
        
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    //Marks:- CustomPicker delegate
    func DidSelect(pickerView: UIPickerView, dataAt Index: Int) {
        
        self.txtPrice.text = currency[Picker.pickerView.selectedRow(inComponent: 0)] as? String ?? ""
        let packagePrice = UserDefaults.standard.object(forKey: "packagePrice")
        let monthlyprice =  UserDefaults.standard.object(forKey: "monthlyPrice")
        let specialPrice =  UserDefaults.standard.object(forKey: "specialpackagePrice")
        if Picker.pickerView.selectedRow(inComponent: 0) == 0{
            self.monthlyPackagePric.text = "$\(monthlyprice!)"
            self.packagePrice.text = "$\(packagePrice!)"
            self.specialPackagePrice.text = "$\(specialPrice!)"
        }
        if Picker.pickerView.selectedRow(inComponent: 0) == 1{
            self.packagePrice.text = "A$\(packagePrice!)"
            self.monthlyPackagePric.text = "A$\(monthlyprice!)"
            self.specialPackagePrice.text = "A$\(specialPrice!)"
        }
        if Picker.pickerView.selectedRow(inComponent: 0) == 2{
            self.packagePrice.text = "£\(packagePrice!)"
            self.monthlyPackagePric.text = "£\(monthlyprice!)"
            self.specialPackagePrice.text = "£\(specialPrice!)"
        }
        if Picker.pickerView.selectedRow(inComponent: 0) == 3{
            self.packagePrice.text = "€\(packagePrice!)"
            self.monthlyPackagePric.text = "€\(monthlyprice!)"
            self.specialPackagePrice.text = "€\(specialPrice!)"
        }
//        self.packagePrice.text = "\(packagePrice!)  \(currency[Picker.pickerView.selectedRow(inComponent: 0)] as? String ?? "")"
        
        view.endEditing(true)
    }
    
    func DidCancel(PickerView: UIPickerView) {
        view.endEditing(true)
    }
    
    //MARK:- UItextfield delegate methods
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.txtPrice = textField
        self.Picker.DataSource = currency
        IQKeyboardManager.shared.isAccessibilityElement = true
    }
    func apiGetSubscriptionDetail()
    {
        ServiceManager.shared.MakeApiCall(ForServiceName: "getSubscriptionDetail", withParameters: nil, withAttachments: nil, withAttachmentName: nil, UploadParameter: nil, httpMethod: .get, ShowLoader: true, ShowTrueFalseValue: true, completionHandler: { (response) in
            let status = response?[0]["status"] as? Int? ?? 0
            let erro_msg = response?[0]["error_msg"] as? String ?? ""
//            let data = response?[0]["data"] as? [[String : Any]] ?? [[:]]
//            let subscriptiondetail = data[0]["SubscriptionDetail"] as? [[String:Any]] ?? [[:]]
//            let duration = subscriptiondetail[1]["duration"] as? String ?? ""
                if status == 1
                {
                   //self.packageDuration.text = duration
                   do{
                       let jsonData = try JSONSerialization.data(withJSONObject: response?[0] ?? [:], options: [])
                       let decode = try JSONDecoder().decode(SubscriptionResponse.self, from: jsonData)
                    self.subscriptionArr = decode.data[0].SubscriptionDetail
                    print(decode.data[0].SubscriptionDetail[0].duration!)
                    self.monthlyPackageTitle.text = decode.data[0].SubscriptionDetail[0].name
                    self.MonthlypackageDuration.text = decode.data[0].SubscriptionDetail[0].duration
                    self.monthlyPackagePric.text = decode.data[0].SubscriptionDetail[0].price
                    self.packageTitle.text = decode.data[0].SubscriptionDetail[1].name
                    self.packageDuration.text = decode.data[0].SubscriptionDetail[1].duration
                    self.packagePrice.text = decode.data[0].SubscriptionDetail[1].price
                    self.specialPackageTitle.text = decode.data[0].SubscriptionDetail[2].name
                    self.specialPackageDuration.text = decode.data[0].SubscriptionDetail[2].duration
                    self.specialPackagePrice.text = decode.data[0].SubscriptionDetail[2].price
                    self.descriptionView.text = decode.data[0].MainDescription
                    UserDefaults.standard.set(decode.data[0].SubscriptionDetail[1].price, forKey: "packagePrice")
                    UserDefaults.standard.set(decode.data[0].SubscriptionDetail[0].price, forKey: "monthlyPrice")
                    UserDefaults.standard.set(decode.data[0].SubscriptionDetail[2].price, forKey: "specialpackagePrice")
                   }catch let error as NSError{
                       print(error.localizedDescription)
                   }
                }
                else
                {
                    UIAlertController().Simplealert(withTitle: erro_msg, Message: "", presentOn: self)
                }
        }, with: nil)
    }
}
extension UIView{
    func makeMePackage(){
        self.clipsToBounds = true
        self.layer.cornerRadius = 8
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 1)
        self.layer.shadowRadius = 3
        self.layer.shadowOpacity = 1
        self.layer.masksToBounds = false;
    }
    func makeMeSelectedPKG(titleLabel : UILabel){
        UIView.animate(withDuration: 0.2, animations: {
            self.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
            titleLabel.backgroundColor = UIColor(red: 86/255, green: 193/255, blue: 195/255, alpha: 1)
            titleLabel.textColor = UIColor.white
        })
        
    }
    
    func makeMeUnselectedPKG(titleLabel : UILabel){
        UIView.animate(withDuration: 0.2, animations: {
            self.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            titleLabel.backgroundColor = UIColor(red: 182/255, green: 239/255, blue: 239/255, alpha: 1)
            titleLabel.textColor = UIColor.black

        })
    }
}

extension UILabel{
    func makeMePackageTitle(){
        self.clipsToBounds = true
        self.layer.cornerRadius = 8
        self.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }
}
