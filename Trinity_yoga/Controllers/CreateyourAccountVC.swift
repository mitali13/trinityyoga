//
//  CreateyourAccountVC.swift
//  Trinity_yoga
//
//  Created by Mandeeppc on 20/03/20.
//  Copyright © 2020 DECODER. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
class CreateyourAccountVC: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var txtConfPass: SkyFloatingLabelTextField!
    @IBOutlet weak var txtPass: SkyFloatingLabelTextField!
    @IBOutlet weak var txtEmailId: SkyFloatingLabelTextField!
    @IBOutlet weak var txtPhoneNo: SkyFloatingLabelTextField!
    @IBOutlet weak var txtFullName: SkyFloatingLabelTextField!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var logInButtonTopY: NSLayoutConstraint!
    @IBOutlet var RegistrationButtonTopY: NSLayoutConstraint!
    @IBOutlet var logInButton: UIButton!
    @IBOutlet var coverImgHeight: NSLayoutConstraint!
    @IBOutlet weak var backbtn: UIButton!
    @IBOutlet weak var registerbtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        txtFullName.changeMeToDefaultColor()
        txtPhoneNo.changeMeToDefaultColor()
        txtEmailId.changeMeToDefaultColor()
        txtPass.changeMeToDefaultColor()
        txtConfPass.changeMeToDefaultColor()
        registerbtn.setTitle("REGISTER", for: .normal)
        titleLabel.text = "Create \nyour account"
        registerbtn.applyRadialGradient(colours: [buttoncolor,buttoncolor2])
        registerbtn.setTitleColor(UIColor.white, for: .normal)
        registerbtn.layer.cornerRadius = registerbtn.frame.height / 2
        registerbtn.clipsToBounds = true
        
        if UIScreen.main.bounds.height < 700{
            coverImgHeight.constant = UIScreen.main.bounds.height / 3
            logInButtonTopY.constant = 15
            RegistrationButtonTopY.constant = 20
        }
        
        let att = NSMutableAttributedString(string: "Already have an Account? Sign In");
        att.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location: 0, length: 24))
        att.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(red: 91/255, green: 196/255, blue: 197/255, alpha: 1), range: NSRange(location: 24, length: 8))
        logInButton.setAttributedTitle(att, for: .normal)
        // Do any additional setup after loading the view.
    let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleGasture))
        swipeLeft.direction = .right
        self.view.addGestureRecognizer(swipeLeft)
    }
    @objc func handleGasture(gesture : UISwipeGestureRecognizer){
        if gesture.direction == .right{
            self.navigationController?.popViewController(animated: true)
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtFullName{
            txtPhoneNo.becomeFirstResponder()
        }
        else if textField == txtPhoneNo{
            txtEmailId.becomeFirstResponder()
        }
        else if textField == txtEmailId{
            txtPass.becomeFirstResponder()
        }
        else if textField == txtPass{
            txtConfPass.becomeFirstResponder()
        }
        else{
            textField.resignFirstResponder()
        }
        return true
    }
    func toValidate()
    {
        if txtFullName.text!.isEmpty
        {
            UIAlertController().Simplealert(withTitle: "Please enter full name.", Message: "", presentOn: self)
        }
        else if (txtFullName.text!.isvalidName == false){
                UIAlertController().Simplealert(withTitle: "Incorrect name.", Message: "", presentOn: self)
        }
        else if txtPhoneNo.text!.isEmpty
        {
            UIAlertController().Simplealert(withTitle: "Please enter phone number.", Message: "", presentOn: self)
        }else if (txtPhoneNo.text!.isPhoneNo == false){
            UIAlertController().Simplealert(withTitle: "Incorrect phone number.", Message: "", presentOn: self)
        }
        else if txtEmailId.text!.isEmpty
        {
            UIAlertController().Simplealert(withTitle: "Please enter email.", Message: "", presentOn: self)
        }else if (txtEmailId.text!.isvalidEmail == false){
            UIAlertController().Simplealert(withTitle: "Incorrect email.", Message: "", presentOn: self)
        }
        if txtPass.text!.isEmpty
        {
            UIAlertController().Simplealert(withTitle: "Please enter password", Message: "", presentOn: self)
        }else if (txtPass.text!.isValidPassword == false){
            UIAlertController().Simplealert(withTitle: "Password are at least 8 character long and contain at least one upercase & one special character.", Message: "", presentOn: self)
        }
       else if txtConfPass.text!.isEmpty
        {
                   UIAlertController().Simplealert(withTitle: "Please enter confirm password", Message: "", presentOn: self)
        }
        else if txtConfPass.text != txtPass.text{
          UIAlertController().Simplealert(withTitle: "Password not match", Message: "", presentOn: self)
        }
        else {
            apiRegisterData()
        }
       
}
    func apiRegisterData(){
        let  params = ["name":txtFullName.text!, "email" : txtEmailId.text!, "phone" : txtPhoneNo.text!, "password" : txtPass.text!, "confirm_password" : txtConfPass.text!]
        ServiceManager.shared.MakeApiCall(ForServiceName: "registerUser", withParameters: params, withAttachments: nil, withAttachmentName: nil, UploadParameter: nil, httpMethod: .post, ShowLoader: true, ShowTrueFalseValue: true, completionHandler: { (response) in
            print(response!)
            let status = response?[0]["status"] as? Int ?? 0
            let erro_msg = response?[0]["error_msg"] as? String ?? ""

            if status == 1
            {
                let data = response?[0]["data"] as? [[String : Any]] ?? [[:]]
                let id = data[0]["id"] as? Int ?? 0
                UserDefaults.standard.set(id, forKey: LoginuserId)
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "WelcomescreenVC") as! WelcomescreenVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else
            {
        UIAlertController().Simplealert(withTitle: erro_msg, Message: "", presentOn: self)
            }
            
        }, with: nil)
    }
    @IBAction func backaction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func Logingaction(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
              self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func registrationaction(_ sender: UIButton) {
        toValidate()
    }
}
extension String{
    var isvalidName: Bool {
        let nameRegEx = "^(([^ ]?)(^[a-zA-Z].*[a-zA-Z]$)([^ ]?))$"
        let nameTest = NSPredicate(format: "SELF MATCHES %@",nameRegEx)
        return nameTest.evaluate(with: self)
    }
    var isvalidEmail: Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{3}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@",emailRegEx)
        return emailTest.evaluate(with: self)
    }
    var isValidPassword: Bool {
        let passTest = NSPredicate(format: "SELF MATCHES %@","^(?=.*[a-z])(?=.*[$@$#!%*?&])[A-Za-z\\d$@$#!%*?&]{8,}")
        return passTest.evaluate(with: self)
    }
    var isPhoneNo : Bool{
        do{
            let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
            let matches = detector.matches(in: self, options: [], range: NSMakeRange(0, self.count))
            if let res = matches.first{
                return res.resultType == .phoneNumber && res.range.location == 0 &&
                    res.range.length == self.count && self.count == 10
            }else {
                return false
            }
        }catch{
            return false
        }
    }
}
