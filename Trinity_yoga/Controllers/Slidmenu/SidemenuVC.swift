//
//  SidemenuVC.swift
//  Trinity_yoga
//
//  Created by Mandeeppc on 20/03/20.
//  Copyright © 2020 DECODER. All rights reserved.
//

import UIKit
import LGSideMenuController

class SidemenuVC: UIViewController, UITableViewDataSource, UITableViewDelegate{
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet var LogoutButtonHeight: NSLayoutConstraint!
    @IBOutlet var logOutButtonBottomY: NSLayoutConstraint!
    @IBOutlet weak var profileimg: UIImageView!
    @IBOutlet weak var menutv: UITableView!
    @IBOutlet weak var logoutbtn: UIButton!
    
    var menu = ["Home","Account","Meditation","Favorite","Notification","Subscription","Settings"]
    var menuimg = [#imageLiteral(resourceName: "home-side-menu-icon"),#imageLiteral(resourceName: "account-side-menu-icon"),#imageLiteral(resourceName: "meditaion-side-menu-icon"),#imageLiteral(resourceName: "fav-slide-menu-icon"),#imageLiteral(resourceName: "bell"),#imageLiteral(resourceName: "subscription"),#imageLiteral(resourceName: "settings")]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        menutv.delegate = self
        menutv.dataSource = self
        profileimg.clipsToBounds = true
        profileimg.layer.cornerRadius = profileimg.frame.height / 2
        profileimg.layer.borderColor = UIColor.white.cgColor
        profileimg.layer.borderWidth = 4
        menutv.register(UINib(nibName: "sidemenucell", bundle: nil), forCellReuseIdentifier: "sidemenucell")
        logoutbtn.backgroundColor = buttoncolor2
        logoutbtn.setTitleColor(UIColor.white, for: .normal)
        
        if UIDevice.modelID == "0"{
            logOutButtonBottomY.constant = 0
            LogoutButtonHeight.constant = 45
        }
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        if UserDefaults.standard.object(forKey: profile)as! String != ""{
        self.lblName.text = UserDefaults.standard.object(forKey: name) as? String
        let userprofile = UserDefaults.standard.object(forKey: profile) as! String
        Utils.setImageFromUrl(imageView: self.profileimg, urlString: profileUrl + userprofile)
        NotificationCenter.default.addObserver(self,selector: #selector(self.getProfile),
                                                           name: Notification.Name("UserProfile"),
                                                          object: nil)
        }else{
             self.lblName.text = UserDefaults.standard.object(forKey: name) as? String
            profileimg.image = #imageLiteral(resourceName: "profile")
        }
       
        }
    @objc func getProfile(_ notification: Notification) {
           self.lblName.text  = notification.userInfo?["name"] as? String
           //UserDefaults.standard.set(Username, forKey: name)
           let user_profile =  (notification.userInfo?["profile"] as? String)
           //UserDefaults.standard.set(user_profile, forKey: profile)
            Utils.setImageFromUrl(imageView: self.profileimg, urlString: profileUrl + user_profile!)
       }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = menutv.dequeueReusableCell(withIdentifier: "sidemenucell") as! sidemenucell
        
        cell.manunamelbl.text = menu[indexPath.row]
        cell.menuimage.image = menuimg[indexPath.row]
        cell.selectionStyle = .none
        if indexPath.row == menu.count - 1{
            cell.seprator.isHidden = true
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        if indexPath.row == 0{
        sideMenuController?.hideLeftView(animated: true, completionHandler: {
            if let vc = self.sideMenuController?.rootViewController as? MyTabbar{
                vc.selectedIndex = 0
            }
            
        })

       }
      else if indexPath.row == 1{
            sideMenuController?.hideLeftView(animated: true, completionHandler: {
                if let vc = self.sideMenuController?.rootViewController as? MyTabbar{
                    vc.selectedIndex = 1
                }
                
            })
        }
        else if indexPath.row == 2{
            sideMenuController?.hideLeftView(animated: true, completionHandler: {
                if let vc = self.sideMenuController?.rootViewController as? MyTabbar{
                    vc.selectedIndex = 4
                }
                
            })
        }
        else if indexPath.row == 3{
            sideMenuController?.hideLeftView(animated: true, completionHandler: {
                if let vc = self.sideMenuController?.rootViewController as? MyTabbar{
                    vc.selectedIndex = 3
                }
                
            })
        }
        else if indexPath.row == 4{
                let vc = storyboard!.instantiateViewController(withIdentifier: "notificationVC") as! NotificationViewController
                vc.hidesBottomBarWhenPushed = true
                sideMenuController?.hideLeftView(animated: true, completionHandler: {
                //self.sideMenuController?.isRootViewStatusBarHidden = true
                (((self.sideMenuController?.rootViewController as! MyTabbar).selectedViewController) as! UINavigationController).show(vc, sender: nil)
                    })
        }
       else if indexPath.row == 5{
            let vc = storyboard!.instantiateViewController(withIdentifier: "SubscriptionVC") as! SubscriptionViewController
            vc.hidesBottomBarWhenPushed = true
            sideMenuController?.hideLeftView(animated: true, completionHandler: {
            //self.sideMenuController?.isRootViewStatusBarHidden = true
            (((self.sideMenuController?.rootViewController as! MyTabbar).selectedViewController) as! UINavigationController).show(vc, sender: nil)
                       })
        }
    }
    
    @IBAction func btnLogoutAction(_ sender: Any) {
        let vc = storyboard!.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        vc.hidesBottomBarWhenPushed = true
        sideMenuController?.hideLeftView(animated: true, completionHandler: {
             UserDefaults.standard.set(nil, forKey: LoginuserId)
          //  UserDefaults.standard.set(nil, forKey: yoga_level_all_Id)

        (((self.sideMenuController?.rootViewController as! MyTabbar).selectedViewController) as! UINavigationController).show(vc, sender: nil) })
       
    }
}
extension UIDevice {
    static let modelID: String = {
           var systemInfo = utsname()
           uname(&systemInfo)
           let machineMirror = Mirror(reflecting: systemInfo.machine)
           let identifier = machineMirror.children.reduce("") { identifier, element in
               guard let value = element.value as? Int8, value != 0 else { return identifier }
               return identifier + String(UnicodeScalar(UInt8(value)))
           }

           func mapToDevice(identifier: String) -> String { // swiftlint:disable:this cyclomatic_complexity
               #if os(iOS)
               switch identifier {
               case "iPod5,1":                                 return "0"
               case "iPod7,1":                                 return "0"
               case "iPod9,1":                                 return "0"
               case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "0"
               case "iPhone4,1":                               return "0"
               case "iPhone5,1", "iPhone5,2":                  return "0"
               case "iPhone5,3", "iPhone5,4":                  return "0"
               case "iPhone6,1", "iPhone6,2":                  return "0"
               case "iPhone7,2":                               return "0"
               case "iPhone7,1":                               return "0"
               case "iPhone8,1":                               return "0"
               case "iPhone8,2":                               return "0"
               case "iPhone9,1", "iPhone9,3":                  return "0"
               case "iPhone9,2", "iPhone9,4":                  return "0"
               case "iPhone8,4":                               return "0"
               case "iPhone10,1", "iPhone10,4":                return "0"
               case "iPhone10,2", "iPhone10,5":                return "0"
               case "iPhone10,3", "iPhone10,6":                return "40"
               case "iPhone11,2":                              return "40"
               case "iPhone11,4", "iPhone11,6":                return "40"
               case "iPhone11,8":                              return "40"
               case "iPhone12,1":                              return "40"
               case "iPhone12,3":                              return "40"
               case "iPhone12,5":                              return "40"
               case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
               case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad (3rd generation)"
               case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad (4th generation)"
               case "iPad6,11", "iPad6,12":                    return "iPad (5th generation)"
               case "iPad7,5", "iPad7,6":                      return "iPad (6th generation)"
               case "iPad7,11", "iPad7,12":                    return "iPad (7th generation)"
               case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
               case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
               case "iPad11,4", "iPad11,5":                    return "iPad Air (3rd generation)"
               case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad mini"
               case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad mini 2"
               case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad mini 3"
               case "iPad5,1", "iPad5,2":                      return "iPad mini 4"
               case "iPad11,1", "iPad11,2":                    return "iPad mini (5th generation)"
               case "iPad6,3", "iPad6,4":                      return "iPad Pro (9.7-inch)"
               case "iPad7,3", "iPad7,4":                      return "iPad Pro (10.5-inch)"
               case "iPad8,1", "iPad8,2", "iPad8,3", "iPad8,4":return "iPad Pro (11-inch)"
               case "iPad8,9", "iPad8,10":                     return "iPad Pro (11-inch) (2nd generation)"
               case "iPad6,7", "iPad6,8":                      return "iPad Pro (12.9-inch)"
               case "iPad7,1", "iPad7,2":                      return "iPad Pro (12.9-inch) (2nd generation)"
               case "iPad8,5", "iPad8,6", "iPad8,7", "iPad8,8":return "iPad Pro (12.9-inch) (3rd generation)"
               case "iPad8,11", "iPad8,12":                    return "iPad Pro (12.9-inch) (4th generation)"
               case "AppleTV5,3":                              return "Apple TV"
               case "AppleTV6,2":                              return "Apple TV 4K"
               case "AudioAccessory1,1":                       return "HomePod"
               case "i386", "x86_64":                          return "\(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "iOS"))"
               default:                                        return identifier
               }
               #elseif os(tvOS)
               switch identifier {
               case "AppleTV5,3": return "Apple TV 4"
               case "AppleTV6,2": return "Apple TV 4K"
               case "i386", "x86_64": return "\(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "tvOS"))"
               default: return identifier
               }
               #endif
           }

           return mapToDevice(identifier: identifier)
       }()
}

