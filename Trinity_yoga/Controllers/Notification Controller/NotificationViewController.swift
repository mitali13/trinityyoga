//
//  NotificationViewController.swift
//  Trinity_yoga
//
//  Created by Dhruv Govani on 13/04/20.
//  Copyright © 2020 DECODER. All rights reserved.
//

import UIKit
struct  NotificationResponse : Decodable {
    let status : Int!
    let error_msg : String!
    let data : [NotificationData]!
}
struct NotificationData : Decodable{
    let id : Int!
    let text : String!
    let profile : String!
    let created_date : String!
}
class NotificationViewController: UIViewController {

    @IBOutlet var notificationTableView: UITableView!
    let user_id = UserDefaults.standard.object(forKey: LoginuserId)
    var notificationArr = [NotificationData]()
    override func viewDidLoad() {
        super.viewDidLoad()
       
        notificationTableView.dataSource = self
        notificationTableView.delegate = self
        notificationTableView.register(UINib(nibName: "NotificationTableViewCell", bundle: nil), forCellReuseIdentifier: "NotificationCell")
        let navLabel = UILabel()
        let navTitle = NSMutableAttributedString(string: "Notification", attributes:[
            NSAttributedString.Key.foregroundColor: UIColor.black,
            NSAttributedString.Key.font: UIFont(name: "Ubuntu", size: 23)!])
        navLabel.attributedText = navTitle
        self.navigationItem.titleView = navLabel
        
        navigationController?.SetupNavWithLeftRightImage(onVc: self, WithTitle: "", leftImage: #imageLiteral(resourceName: "back"), RightImage: nil, leftAction: #selector(backAction), Image: nil)
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleGasture))
               swipeLeft.direction = .right
               self.view.addGestureRecognizer(swipeLeft)
               
        }
        @objc func handleGasture(gesture : UISwipeGestureRecognizer){
               if gesture.direction == .right{
                   self.navigationController?.popViewController(animated: true)
               }
        }

    override func viewWillAppear(_ animated: Bool) {
         apiGetNotification()
    }
   @objc func backAction(){
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
   func apiGetNotification(){
           let params = ["user_id": user_id!]
           ServiceManager.shared.MakeApiCall(ForServiceName: "notification_list", withParameters: params, withAttachments: nil, withAttachmentName: nil, UploadParameter: nil, httpMethod: .get, ShowLoader: true, ShowTrueFalseValue: true, completionHandler: { (response) in
               let status = response?[0]["status"] as? Int ?? 0
               let error_msg = response?[0]["error_msg"] as? String ?? ""
               if status == 1{
                   do{
                       let jsonData = try JSONSerialization.data(withJSONObject: response?[0] ?? [:], options: [])
                       let decode = try JSONDecoder().decode(NotificationResponse.self, from: jsonData)
                       self.notificationArr = decode.data!
                       print(self.notificationArr)
                       
                       self.notificationTableView.reloadData()
                   }catch let error as NSError{
                       print(error.localizedDescription)
                   }
               }else{
                   UIAlertController().Simplealert(withTitle: error_msg, Message: "", presentOn: self)
               }
           }, with: nil)
       }

}
extension NotificationViewController : UITableViewDataSource, UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notificationArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = notificationTableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as! NotificationTableViewCell
       
        cell.notificationDescription.text = notificationArr[indexPath.row].text
        cell.notificationTime.text = notificationArr[indexPath.row].created_date
        Utils.setImageFromUrl(imageView: cell.notificationImage, urlString: profileUrl + notificationArr[indexPath.row].profile)
        //cell.configCell(with: notificationItems[indexPath.row])
        print(profileUrl + notificationArr[indexPath.row].profile)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}
