//
//  NotificationTableViewCell.swift
//  Trinity_yoga
//
//  Created by Dhruv Govani on 13/04/20.
//  Copyright © 2020 DECODER. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {
    @IBOutlet var notificationView: UIView!
    @IBOutlet var notificationDescription: UILabel!
    @IBOutlet var notificationImage: UIImageView!
    @IBOutlet var notificationTime: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        notificationImage.clipsToBounds = true
        notificationImage.layer.cornerRadius = notificationImage.frame.height / 2
        notificationView.clipsToBounds = true
        notificationView.layer.cornerRadius = 8
        notificationView.backgroundColor = UIColor.white
        notificationView.layer.shadowColor = UIColor.lightGray.cgColor
        notificationView.layer.shadowOffset = CGSize(width: 0, height: 1)
        notificationView.layer.shadowRadius = 3
        notificationView.layer.shadowOpacity = 1
        notificationView.layer.masksToBounds = false;
        // Configure the view for the selected state
    }
    
//    func configCell(with model : Notification){
//        notificationImage.image = model.notificationImage
//        notificationDescription.text = model.notificationDesc
//        notificationTime.text = model.notificationTime
//    }
    
}
