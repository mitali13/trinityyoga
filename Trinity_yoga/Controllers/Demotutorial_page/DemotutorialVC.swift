//
//  DemotutorialVC.swift
//  Trinity_yoga
//
//  Created by Mandeeppc on 20/03/20.
//  Copyright © 2020 DECODER. All rights reserved.
//
import UIKit
import LGSideMenuController
import AVKit
import AVFoundation
struct YogaTutorialResponse : Decodable{
    let status : Int!
    let error_msg : String!
    let data : [YogaTutorialData]!
}
struct YogaTutorialData : Decodable{
    let id : Int!
    let title : String!
    let description : String!
    let video : String!
    let status : Int!
    let totallike : Int!
    let totalunlike : Int!
    let mylike : Int!
    let myunlike : Int!
    let videoduration : String!
}
class DemotutorialVC: UIViewController {
    
    var yogaTurorialArr = [YogaTutorialData]()
    @IBOutlet var demoTutorialsTableView: UITableView!
    @IBOutlet weak var demobtn: UIButton!
    //    var video: [Video] = vide
    var isPlay = true
    var tot_like,tot_dislike,my_like,my_unlike : Int!
    var like_type: Int!
    var videoId : Int!
    var previousIndex : Int!
    @IBOutlet weak var playbtn: UIButton!
    let user_id = UserDefaults.standard.object(forKey: LoginuserId)
    override func viewDidLoad() {
        super.viewDidLoad()
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback, mode: AVAudioSession.Mode.default, options: [])
        }
        catch {
            print("Setting category to AVAudioSessionCategoryPlayback failed.")
        }
       
        demobtn.applyRadialGradient(colours: [buttoncolor,buttoncolor2])
        demobtn.clipsToBounds = true
        demobtn.layer.cornerRadius = demobtn.frame.height / 2
        demobtn.setTitle("Demo Tutorials", for: .normal)
        demobtn.setTitleColor(UIColor.white, for: .normal)
        demoTutorialsTableView.dataSource = self
        demoTutorialsTableView.delegate = self
        demoTutorialsTableView.register(UINib(nibName: "TutorialVideoTableViewCell", bundle: nil), forCellReuseIdentifier: "TutorialCell")
    }
    override func viewWillAppear(_ animated: Bool) {
         apiGetDemoTutorial()
        self.navigationController?.navigationBar.isHidden = true
        self.tabBarController?.tabBar.isHidden = true
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        //player.pause()
    }
    func apiGetLikeDislike(){
        let params = ["user_id": user_id!,"video_id" : videoId!,"type" : like_type!]
        ServiceManager.shared.MakeApiCall(ForServiceName: "likeAndUnlike", withParameters: params, withAttachments: nil, withAttachmentName: nil, UploadParameter: nil, httpMethod: .get, ShowLoader: false, ShowTrueFalseValue: true, completionHandler: { (response) in
            let status = response?[0]["status"] as? Int ?? 0
            let error_msg = response?[0]["error_msg"] as? String ?? ""
            if status == 1{
                self.apiGetDemoTutorial()
            }else{
                UIAlertController().Simplealert(withTitle: error_msg, Message: "", presentOn: self)
            }
        }, with: nil)
    }
    func apiGetDemoTutorial(){
        let params = ["user_id": user_id!]
        ServiceManager.shared.MakeApiCall(ForServiceName: "demo_Tutorial", withParameters: params, withAttachments: nil, withAttachmentName: nil, UploadParameter: nil, httpMethod: .get, ShowLoader: true, ShowTrueFalseValue: true, completionHandler: { (response) in
            let status = response?[0]["status"] as? Int ?? 0
            let error_msg = response?[0]["error_msg"] as? String ?? ""
            if status == 1{
                do{
                    let jsonData = try JSONSerialization.data(withJSONObject: response?[0] ?? [:], options: [])
                    let decode = try JSONDecoder().decode(YogaTutorialResponse.self, from: jsonData)
                    self.yogaTurorialArr = decode.data!
                    //print(self.yogaTurorialArr)
                    self.demoTutorialsTableView.reloadData()
                }catch let error as NSError{
                    print(error.localizedDescription)
                }
            }else{
                UIAlertController().Simplealert(withTitle: error_msg, Message: "", presentOn: self)
            }
        }, with: nil)
    }
    @IBAction func demotutorialsaction(_ sender: UIButton) {
        let sidemenu = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SidemenuVC") as! SidemenuVC
        let app = UIApplication.shared.delegate as! AppDelegate
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MyTabbar") as! MyTabbar
        vc.selectedIndex = 0
        let mainVC = LGSideMenuController.init(rootViewController: vc, leftViewController: sidemenu, rightViewController: nil)
        mainVC.leftViewWidth = 300
        app.window?.rootViewController = mainVC
        //          app.window?.rootViewController = vc
        app.window?.makeKeyAndVisible()
        //    if #available(iOS 13.0, *) {
        //                let sidemenu = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SidemenuVC") as! SidemenuVC
        //                let app = UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate
        //                let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MyTabbar") as! MyTabbar
        //                vc.selectedIndex = 0
        //                 let mainVC = LGSideMenuController.init(rootViewController: vc, leftViewController: sidemenu,     rightViewController: nil)
        //                mainVC.leftViewWidth = 300
        //                app?.window?.rootViewController = mainVC
        //                app?.window?.makeKeyAndVisible()
        //            } else{
        // }
    }
    @objc func playAction(_ sender : UIButton){
        let vc = self.storyboard?.instantiateViewController(identifier: "mediaplayervc")as! mediaplayervc
               self.navigationController?.isNavigationBarHidden = true
               let urlStr = yogaTurorialArr[sender.tag].video
               vc.vdoUrl = videoUrl + urlStr!
               vc.video_id = yogaTurorialArr[sender.tag].id
               vc.my_like = yogaTurorialArr[sender.tag].mylike
               vc.my_unlike = yogaTurorialArr[sender.tag].myunlike
               vc.tot_like = yogaTurorialArr[sender.tag].totallike
               vc.tot_unlike = yogaTurorialArr[sender.tag].totalunlike
               self.navigationController?.pushViewController(vc, animated: true)
    }
//    @objc func FullScreenAction(_ sender : UIButton){
//        let urlStr = yogaTurorialArr[sender.tag].video
//        let vdoUrl = videoUrl + urlStr!
//        let videopathurl = URL(string : vdoUrl)
//        print(videopathurl!)
//        player = AVPlayer(url: videopathurl!)
//        playercontroller.player = player
//        self.present(playercontroller, animated: true, completion: nil)
//    }
    @objc func likeAction(sender : UIButton){
       videoId = yogaTurorialArr[sender.tag].id
       my_like = yogaTurorialArr[sender.tag].mylike
        if my_like == 0{
            like_type = 1
            apiGetLikeDislike()
            }
        else if my_like == 1{
            like_type = 0
            apiGetLikeDislike()
        }
    }
    @objc func dislikeAction(sender : UIButton){
        videoId = yogaTurorialArr[sender.tag].id
        my_unlike = yogaTurorialArr[sender.tag].myunlike
        if my_unlike == 0{
            like_type = 2
            apiGetLikeDislike()
        }
        else if my_unlike == 1{
            like_type = 0
            apiGetLikeDislike()
        }
    }
}
extension DemotutorialVC : UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // print(yogaTurorialArr.count)
        return yogaTurorialArr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = demoTutorialsTableView.dequeueReusableCell(withIdentifier: "TutorialCell", for: indexPath) as! TutorialVideoTableViewCell
        
        cell.videoTitle.text = yogaTurorialArr[indexPath.row].title
        cell.videoDescription.text = yogaTurorialArr[indexPath.row].description
        tot_like = yogaTurorialArr[indexPath.row].totallike
        tot_dislike = yogaTurorialArr[indexPath.row].totalunlike
        my_like = yogaTurorialArr[indexPath.row].mylike
        my_unlike = yogaTurorialArr[indexPath.row].myunlike
        if my_like == 1{
            cell.likeButton.setImage(#imageLiteral(resourceName: "fill_like"), for: .normal)
        }else{
              cell.likeButton.setImage(#imageLiteral(resourceName: "like"), for: .normal)
        }
        if my_unlike == 1{
            cell.deslikeButton.setImage(#imageLiteral(resourceName: "fill_dislike"), for: .normal)
        }else{
              cell.deslikeButton.setImage(#imageLiteral(resourceName: "dislike"), for: .normal)
        }
        cell.likeButton.setTitle(String(tot_like), for: .normal)
        cell.deslikeButton.setTitle(String(tot_dislike), for: .normal)
        cell.videoDuration.text = yogaTurorialArr[indexPath.row].videoduration
        let urlStr = yogaTurorialArr[indexPath.row].video
        let vdoUrl = videoUrl + urlStr!
                  //  print(vdoUrl)
        let videopathurl = URL(string : vdoUrl)
        let asset = AVURLAsset(url: videopathurl!, options: nil)
        let generator = AVAssetImageGenerator(asset: asset)
        generator.appliesPreferredTrackTransform = true
         do {
            cell.videoThumbnail.image = try UIImage(cgImage: generator.copyCGImage(at: CMTime(seconds: 0, preferredTimescale: 1), actualTime: nil))
            } catch let e as NSError {
                       print("Error: \(e.localizedDescription)")
        }
        cell.playButton.tag = indexPath.row
        cell.btnFullScreen.isHidden = true
       // cell.btnFullScreen.tag = indexPath.row
        cell.playButton.addTarget(self, action: #selector(playAction), for: .touchUpInside)
     //   cell.btnFullScreen.addTarget(self, action:  #selector(FullScreenAction), for: .touchUpInside)
        cell.likeButton.tag = indexPath.row
        cell.deslikeButton.tag = indexPath.row
        cell.likeButton.addTarget(self, action: #selector(likeAction), for: .touchUpInside)
        cell.deslikeButton.addTarget(self, action: #selector(dislikeAction), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

