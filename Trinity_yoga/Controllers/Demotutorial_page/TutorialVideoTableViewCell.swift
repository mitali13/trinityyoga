//
//  TutorialVideoTableViewCell.swift
//  Trinity_yoga
//
//  Created by Dhruv Govani on 08/04/20.
//  Copyright © 2020 DECODER. All rights reserved.
//

import UIKit

class TutorialVideoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var viewVideoVc: DGVideoPlayer!

    @IBOutlet weak var btnAddToFav: UIButton!
    @IBOutlet weak var btnFullScreen: UIButton!
    @IBOutlet var likeButton: UIButton!
    @IBOutlet var videoThumbnail: UIImageView!
    @IBOutlet var videoDuration: UILabel!
    @IBOutlet var deslikeButton: UIButton!
    @IBOutlet var playButton: UIButton!
    @IBOutlet var videoDescription: UILabel!
    @IBOutlet var videoTitle: UILabel!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        viewVideoVc.clipsToBounds = true
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
//    func configCell(with model : YogaTutorial){
//        likeButton.titleLabel?.text = String(model.likes.shorted())
//        videoThumbnail.image = model.videoThumbnail
//        videoDuration.text = "\(model.videoDuration[0]) Min"
//        
//        deslikeButton.titleLabel?.text = String(model.deslikes)
//        videoDescription.text = model.videDescription
//        videoTitle.text = model.videoTitle
//    }
    
   
}
