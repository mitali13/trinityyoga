//
//  InterestVC2.swift
//  Trinity_yoga
//
//  Created by Maitree on 27/03/20.
//  Copyright © 2020 DECODER. All rights reserved.
//

import UIKit

class InterestVC2: UIViewController, UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    @IBOutlet var collectionviewTopY: NSLayoutConstraint!
    
    @IBOutlet weak var applybutton: UIButton!
    @IBOutlet weak var interestcollection: UICollectionView!
    
     var yogaItems = [Interested]()
    var collectionViewLayout : UICollectionViewFlowLayout!
    var user_id : Int!
    var interest_id : Int!
    override func viewDidLoad() {
        super.viewDidLoad()
        user_id = UserDefaults.standard.object(forKey: LoginuserId) as? Int ?? 0
        apiGetInterestList()
        if UIScreen.main.bounds.height < 700 {
                   collectionviewTopY.constant = 10
               }
        self.interestcollection.delegate = self
        self.interestcollection.dataSource = self
        interestcollection.register(UINib(nibName: "Interest_cell", bundle: nil), forCellWithReuseIdentifier: "Interest_cell")
        applybutton.layer.cornerRadius = applybutton.frame.height / 2
        applybutton.applyRadialGradient(colours: [buttoncolor,buttoncolor2])
        applybutton.clipsToBounds = true
        applybutton.setTitle("CONTINUE", for: .normal)
        applybutton.setTitleColor(UIColor.white, for: .normal)
        
        // Do any additional setup after loading the view.
    let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleGasture))
        swipeLeft.direction = .right
        self.view.addGestureRecognizer(swipeLeft)
    }
    @objc func handleGasture(gesture : UISwipeGestureRecognizer){
        if gesture.direction == .right{
            self.navigationController?.popViewController(animated: true)
        }
    }
    override func viewDidLayoutSubviews() {
           super.viewDidLayoutSubviews()
           configLayout()
       }
       
       func configLayout() {
           
           if collectionViewLayout == nil {
               collectionViewLayout = UICollectionViewFlowLayout()
               let interitemspacing : CGFloat = 1
               let lineSpacing : CGFloat = 8
       
               
               collectionViewLayout.minimumInteritemSpacing = interitemspacing
               collectionViewLayout.minimumLineSpacing = lineSpacing
               collectionViewLayout.scrollDirection = .vertical
               //collectionViewLayout.sectionInset = UIEdgeInsets.zero
               interestcollection.setCollectionViewLayout(collectionViewLayout, animated: true)
           }
           
           
       }
    
    func apiGetInterestList(){
        ServiceManager.shared.MakeApiCall(ForServiceName: "three_category_list", withParameters: nil, withAttachments: nil, withAttachmentName: nil, UploadParameter: nil, httpMethod: .get, ShowLoader: true, ShowTrueFalseValue: true, completionHandler: { (response) in
            //print(response!)
            let status = response?[0]["status"] as? Int ?? 0
            let erro_msg = response?[0]["error_msg"] as? String ?? ""
            if status == 1
            {
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: response?[0] ?? [:], options: [])
                    let decode = try JSONDecoder().decode(InterestedResponse.self, from: jsonData)
                    
                    self.yogaItems = decode.data?[0].interested ?? []
                    self.interestcollection.reloadData()
                    
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
            else
            {
                UIAlertController().Simplealert(withTitle: erro_msg, Message: "", presentOn: self)
            }
        }, with: nil)
    }
    func apiSetInterestList(){
       
        let params = ["user_id": user_id!,"yoga_level_all_id":interest_id!]
        ServiceManager.shared.MakeApiCall(ForServiceName: "yogaLevelAllIdSave", withParameters: params, withAttachments: nil, withAttachmentName: nil, UploadParameter: nil, httpMethod: .get, ShowLoader: true, ShowTrueFalseValue: true, completionHandler: { (response) in
            print(response!)
            let status = response?[0]["status"] as? Int ?? 0
            let erro_msg = response?[0]["error_msg"] as? String ?? ""
            if status == 1
            {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "InterestVC3") as! InterestVC3
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else
            {
                UIAlertController().Simplealert(withTitle: erro_msg, Message: "", presentOn: self)
            }
            
        }, with: nil)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return yogaItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = interestcollection.dequeueReusableCell(withReuseIdentifier: "Interest_cell", for: indexPath) as! Interest_cell
        cell.interestLabel.text = yogaItems[indexPath.row].name
        return cell
    }
         func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
     //        let screenSize: CGRect = UIScreen.main.bounds
     //              let screenWidth = 120
     //              return CGSize(width: screenWidth, height: (screenWidth/3))
             
             let width = ((interestcollection.bounds.width - 10) / 3)
             
             return CGSize(width: width, height: 95)
             
         }
    
     func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            let cell = interestcollection.cellForItem(at: indexPath) as! Interest_cell
            
                cell.checkImage.image = UIImage(named: "check")
                interest_id = yogaItems[indexPath.row].id
                UserDefaults.standard.set(interest_id, forKey: "interest_id")
                print(interest_id!)
                cell.checkImage.isHidden = false
            
    //        for i in 0..<JourneyItems.count{
    //            checkArr[i].isChecked = false
    //        }
    //        checkArr[indexPath.row].isChecked = true
            //interestcollection.reloadData()
        }
        func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
            let cell = interestcollection.cellForItem(at: indexPath) as! Interest_cell
            cell.checkImage.isHidden = true
            
        }
    
    
    @IBAction func continueaction(_ sender: UIButton) {
        if interest_id == nil{
             UIAlertController().Simplealert(withTitle: "Please Select what are you interested in yoga.", Message: "", presentOn: self)
        }else{
            apiSetInterestList()
        }
    }
    
    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)

    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
